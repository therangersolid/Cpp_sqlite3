DIRECTORY = $(addprefix Debug/,$(sort $(dir $(wildcard src/*/))))
CC := gcc
CCFLAGS :=
CC_SRCS := $(shell find -name '*.c')
CC_OBJS := $(addprefix Debug/,$(patsubst ./%,%,$(patsubst %.c,%.o,$(CC_SRCS))))

CXX := g++
CXXFLAGS := -std=c++0x -I/usr/local/include -I$(HOME)/include -O3 -g0 -Wall -fmessage-length=0 -march=native -fopenmp

CXX_SRCS := $(shell find -name '*.cpp')
CXX_OBJS := $(addprefix Debug/,$(patsubst ./%,%,$(patsubst %.cpp,%.o,$(CXX_SRCS))))
USER_OBJS := 
LIBS := -L/usr/local/lib -L$(HOME)/lib -lsqlite3 -lgomp -lpthread

EXECUTABLES := Cpp_sqlite3
# Add inputs and outputs from these tool invocations to the build variables 

# All Target
all: cpu
cpu: clean makeDirectory exe
	
$(CC_OBJS) : %.o :
	$(CC) $(CCFLAGS) -c $(patsubst Debug/%,%,$(patsubst %.o,%.c,$@)) -o $@

$(CXX_OBJS) : %.o :
	$(CXX) $(CXXFLAGS) -c $(patsubst Debug/%,%,$(patsubst %.o,%.cpp,$@)) -o $@


exe: $(CC_OBJS) $(CXX_OBJS)
	@echo 'Building target: CPU'# $@
	@echo 'Invoking: GCC C++ Linker'
	$(CXX) $(CXXFLAGS) -o Debug/$(EXECUTABLES) $(CC_OBJS) $(CXX_OBJS) $(USER_OBJS) $(LIBS)
	@echo 'Finished building target: $(EXECUTABLES)'
	@echo ' '

makeDirectory:
	mkdir -p Debug/ $(DIRECTORY)

clean :
	rm -R Debug | echo ''

err :
	@echo 'Error make command! Either use "make cpu" or "make cuda"!!'

.PHONY: all clean
