//============================================================================
// Name        : Cpp_sqlite3.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Taken from https://stackoverflow.com/questions/31146713/sqlite3-exec-callback-function-clarification
//============================================================================

#include <stdio.h>
#include <sqlite3.h>
#include "avf_pvf_table.h"
#include "avf_instruction_table.h"
#include <pthread.h>

#include "avf_cuda_exec.h"
#include "avf_cuda_kernel.h"

#include <unistd.h>
#include <syscall.h>
#include <vector>

#include <assert.h>

#include <string.h>
#include <omp.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <chrono>
////
int avf_range;

int flatten3Dto1Dcta(int x, int y, int z, int gridDim_x, int gridDim_y) {
	return x + gridDim_x * y + gridDim_x * gridDim_y * z;
}

int flatten3Dto1Dtid(int x, int y, int z, int blockDim_x, int blockDim_y) {
	return blockDim_x * blockDim_y * z + blockDim_x * y + x;
}

template<class _Tp>
_Tp maxValue(_Tp a, _Tp b) {
	if (a > b) {
		return a;
	} else {
		return b;
	}
}

template<class _Tp>
_Tp minValue(_Tp a, _Tp b) {
	if (a > b) {
		return b;
	} else {
		return a;
	}
}
////

sqlite3* db;
pthread_mutex_t avf_lock;
omp_lock_t openmp_lock;

void processInstructions(int cudaExecId, int shader_id, int warp_id,
		float* instAVFs, int instAVFs_size) {
	// Get the entire list for the table:
	std::vector<InstPvfTable*> instPvfTables;
	inst_pvf_table_read_list(db, &instPvfTables, cudaExecId, shader_id,
			warp_id);
	int instPvfTablesSize = instPvfTables.size();
	printf("Number of Inst record at [%i,%i,%i]= %i\n", cudaExecId, shader_id,
			warp_id, instPvfTablesSize);
	/**
	 *  Calculate the avf.
	 *  3 Steps:
	 *  1. Close each instruction by adding destination 0 at the back
	 *  2. If the first case is not a destination, then set it as destination, then set the pvf as 0.
	 *  3. Adjust the pvf to start from destination on all nodes
	 *  The fourth step will be done in the database
	 *  4. Calculate the avf based on the preferred intervals.
	 */
	if (instPvfTablesSize > 0) {
		/**
		 * 1st step.
		 * The +avfSamplingInterval means that the remaining result that less than the sampling
		 * interval also printed out.
		 */
		InstPvfTable* instPvfTable = new InstPvfTable(0, true, 0.0f); //add a dst
		memcpy(instPvfTable, instPvfTables.at(instPvfTablesSize - 1),
				sizeof(InstPvfTable)); // Shallow copy
		instPvfTable->clock_cycle += avf_range;
//		printf("[%i,%i] clock_cycle = %lli\n", instPvfTable->shader_id,
//				instPvfTable->warp_id, instPvfTable->clock_cycle);
//		printf("Shader id are %i\n", instPvfTable->shader_id);
		fflush(stdout);

		//		pvf_table_insert(db, pvfTable, tot_block_id, tot_thread_id,
		//				tot_reg_id); do not modify the table
		instPvfTables.push_back(instPvfTable);

		// 2nd step:
		instPvfTables.at(0)->dst_src = true;
		instPvfTables.at(0)->reg_pvf = 0.0f;
//		inst_pvf_table_update(db, instPvfTables.at(0)); do not modify the table

		///////////////////////////////////////////////////////////////////////////////
		// 3rd step:
		/**
		 * Sample case: [src,1.0] [src,0.5] [dst,0.0] [src,0.5] [src,1.0] [src,0.5] [src,0.2] [dst,0.0] [src,0.5] [src,0.5] [src,1.0] [src,0.5] [dst,0.0]
		 *
		 * Output:      [dst,0.5] [dst,0.0] [dst,1.0] [dst,1.0] [dst,0.7] [dst,0.2] [dst,0.0] [dst,1.0] [dst,1.0] [dst,1.0] [dst,0.5] [dst,0.0] [dst,0.0]
		 *
		 * Note:
		 * 1. Since the sample case started with src instead of dst (The case of
		 *    with the load instruction), the first src is considered a dst.
		 *    Therefore, in the case above, you see [dst,0.5]
		 *    (Algorithm similar to convolution)
		 */
		float tempPvf = 0.0f;
		float tempPvf2 = 0.0f;
		for (int i = instPvfTablesSize - 1; i >= 0; --i) {
			if (instPvfTables.at(i)->dst_src) {
				tempPvf2 = 0.0f;
			} else {
				tempPvf2 = instPvfTables.at(i)->reg_pvf + tempPvf;

			}
			instPvfTables.at(i)->reg_pvf = tempPvf;
			if (instPvfTables.at(i)->reg_pvf >= 1) {
				instPvfTables.at(i)->reg_pvf = 1;
				if (instPvfTables.at(i)->dst_src == false) {
					for (int j = i - 1;
							j >= 0 && (!instPvfTables.at(j)->dst_src); j--) {
//						inst_pvf_table_delete(db, instPvfTables.at(j)); do not modify the table
//						printf("Deleting %i, %i, %i\n", instPvfTables.at(j)->shader_id, instPvfTables.at(j)->warp_id,instPvfTables.at(j)->id);
						delete (instPvfTables.at(j));
//						printf("Deleting0 : %i\n",j);
						instPvfTables.erase(instPvfTables.begin() + j);
						--i;
						--instPvfTablesSize;
					}
				}
			}
			if (((i + 1) < instPvfTablesSize) && (!instPvfTables.at(i)->dst_src)
					&& (instPvfTables.at(i + 1)->clock_cycle
							== instPvfTables.at(i)->clock_cycle)) {
//				printf("delete =%i\n", instPvfTables.at(i)->id);
//				inst_pvf_table_delete(db, instPvfTables.at(i)); do not modify the table
				delete (instPvfTables.at(i));
//				printf("Deleting1 : %i\n",i);
				instPvfTables.erase(instPvfTables.begin() + i);
				--instPvfTablesSize;
			} else {
//				inst_pvf_table_update(db, instPvfTables.at(i)); do not modify the table
			}
			tempPvf = tempPvf2;
		}
		/// AVF grapf for instPvfTables now!!
		InstPvfTable* instPvfTable0 = instPvfTables.at(0);
		int i = instPvfTable0->clock_cycle / avf_range;
		int n_samples = 0;

		float avf_sum = 0;
		for (int z = 0; z < i; ++z) {
			//						printf("AVF [%i,%i,%i][%i] = %.3f\n", cudaExec->id,
			//								shaderId, warpId, (z + 1) * avf_range - 1, 0);

			//////////////////////////////////
			n_samples++;
//			if ((n_samples - 1) >= instAVFs_size) {
//				printf("error inst 0 = %i\n", (n_samples - 1));
//				fflush(stdout);
//				assert(0);
//			} else {
			omp_set_lock(&openmp_lock);
			instAVFs[n_samples - 1] += avf_sum / avf_range;
			omp_unset_lock(&openmp_lock);
//				if (instAVFs[n_samples - 1] < 0) {
//					printf("error inst 0 = %f,%f\n", instAVFs[n_samples - 1],
//							(avf_sum / avf_range));
//					fflush(stdout);
//					assert(0);
//				}
//			}
			avf_sum = 0;
			/////////////////////////////////
		}
		int range = (i + 1) * avf_range;

		for (int i = 1; i < instPvfTables.size(); ++i) {
			InstPvfTable* instPvfTable1 = instPvfTables.at(i);
			InstPvfTable* instPvfTable2 = instPvfTables.at(i - 1);
			if (instPvfTable1->clock_cycle < range) {
				//							printf("Entering < range\n");

				avf_sum += instPvfTable2->reg_pvf
						* (instPvfTable1->clock_cycle
								- maxValue(instPvfTable2->clock_cycle,
										(long long int) range - avf_range));
				//							printf("AVF [%lli - %i] = %.3f\n",
				//									maxValue(instPvfTable2->clock_cycle,
				//											(long long int) range - avf_range),
				//									instPvfTable1->clock_cycle,
				//									instPvfTable2->reg_pvf);
			} else {
				//							printf("Entering else range\n");

				avf_sum += instPvfTable2->reg_pvf
						* (range
								- maxValue(instPvfTable2->clock_cycle,
										(long long int) range - avf_range));
				//							printf("AVF [%i - %lli] = %.3f\n",
				//									maxValue(instPvfTable2->clock_cycle,
				//											(long long int) range - avf_range),
				//									range - 1, instPvfTable2->reg_pvf);
				//							printf("AVF Sum = %.3f\n", avf_sum / avf_range);
				//////////////////////////////////
				n_samples++;

//				if ((n_samples - 1) >= instAVFs_size) {
//					printf("error inst 1 = %i\n", (n_samples - 1));
//					fflush(stdout);
//					assert(0);
//				} else {
				omp_set_lock(&openmp_lock);
				instAVFs[n_samples - 1] += avf_sum / avf_range;
				omp_unset_lock(&openmp_lock);
//					if (instAVFs[n_samples - 1] < 0) {
//						printf("error inst 1 = %f,%f\n",
//								instAVFs[n_samples - 1], (avf_sum / avf_range));
//						fflush(stdout);
//						assert(0);
//					}
//				}
				avf_sum = 0;
				/////////////////////////////////

				range += avf_range;
				--i;
			}

		}
		//					printf("AVF Sum = %.3f\n", avf_sum / avf_range);
		//////////////////////////////////
		n_samples++;
//		if ((n_samples - 1) >= instAVFs_size) {
//			printf("error inst 2 = %i\n", (n_samples - 1));
//			fflush(stdout);
//			assert(0);
//		} else {
		omp_set_lock(&openmp_lock);

		instAVFs[n_samples - 1] += avf_sum / avf_range;
		omp_unset_lock(&openmp_lock);
//			if (instAVFs[n_samples - 1] < 0) {
//				printf("error inst 2 = %f,%f\n", instAVFs[n_samples - 1],
//						(avf_sum / avf_range));
//				fflush(stdout);
//				assert(0);
//			}
//		}
		avf_sum = 0;
		/////////////////////////////////
		/// Freeing the vector:
		for (int i = 0; i < instPvfTables.size(); ++i) {
			delete (instPvfTables.at(i));
//			printf("Deleting2 : %i\n",i);
		}

	}

}

void processAVF(int cudaExecId, int blockId, int threadId, int regId,
		int tot_block_id, int tot_thread_id, int max_reg_id, float* AVFs,
		int AVFs_size) {
	// Get the entire list for the table:
	std::vector<PvfTable*> pvfTables;
	pvf_table_read_list(db, &pvfTables, cudaExecId, blockId, threadId, regId);
	int pvfTablesSize = pvfTables.size();
	printf("Number of Reg record at [%i,%i,%i,%i]= %i\n", cudaExecId, blockId,
			threadId, regId, pvfTablesSize);
	/**
	 *  Calculate the avf.
	 *  3 Steps:
	 *  1. Close each register by adding destination 0 at the back
	 *  2. If the first case is not a destination, then set it as destination, then set the pvf as 0.
	 *  3. Adjust the pvf to start from destination on all nodes
	 *  The fourth step will be done in the database
	 *  4. Calculate the avf based on the preferred intervals.
	 */
	if (pvfTablesSize > 0) {
		/**
		 * 1st step.
		 * The +avfSamplingInterval means that the remaining result that less than the sampling
		 * interval also printed out.
		 */
		PvfTable* pvfTable = new PvfTable(0, true, 0.0f); //add a dst
		memcpy(pvfTable, pvfTables.at(pvfTablesSize - 1), sizeof(PvfTable)); // Shallow copy
		pvfTable->clock_cycle += avf_range;

//		printf("[%i,%i,%i] clock_cycle = %lli\n", pvfTable->block_id,
//				pvfTable->thread_id, pvfTable->reg_id, pvfTable->clock_cycle);
//		printf("Shader id are %i\n", pvfTable->shader_id);
		fflush(stdout);

//		pvf_table_insert(db, pvfTable, tot_block_id, tot_thread_id,
//				tot_reg_id); do not modify the table
		pvfTables.push_back(pvfTable);

		// 2nd step:
		pvfTables.at(0)->dst_src = true;
		pvfTables.at(0)->reg_pvf = 0.0f;
//		pvf_table_update(db, pvfTables.at(0), tot_reg_id); do not modify the table

		///////////////////////////////////////////////////////////////////////////////
		// 3rd step:
		/**
		 * Sample case: [src,1.0] [src,0.5] [dst,0.0] [src,0.5] [src,1.0] [src,0.5] [src,0.2] [dst,0.0] [src,0.5] [src,0.5] [src,1.0] [src,0.5] [dst,0.0]
		 *
		 * Output:      [dst,0.5] [dst,0.0] [dst,1.0] [dst,1.0] [dst,0.7] [dst,0.2] [dst,0.0] [dst,1.0] [dst,1.0] [dst,1.0] [dst,0.5] [dst,0.0] [dst,0.0]
		 *
		 * Note:
		 * 1. Since the sample case started with src instead of dst (The case of
		 *    with the load instruction), the first src is considered a dst.
		 *    Therefore, in the case above, you see [dst,0.5]
		 *    (Algorithm similar to convolution)
		 */
		float tempPvf = 0.0f;
		float tempPvf2 = 0.0f;
		for (int i = pvfTablesSize - 1; i >= 0; --i) {
			if (pvfTables.at(i)->dst_src) {
				tempPvf2 = 0.0f;
			} else {
				tempPvf2 = pvfTables.at(i)->reg_pvf + tempPvf;
			}
			pvfTables.at(i)->reg_pvf = tempPvf;
			if (pvfTables.at(i)->reg_pvf >= 1) {
				pvfTables.at(i)->reg_pvf = 1;
				if (pvfTables.at(i)->dst_src == false) {
					for (int j = i - 1; j >= 0 && (!pvfTables.at(j)->dst_src);
							j--) {
//						pvf_table_delete(db, pvfTables.at(j), tot_reg_id); do not modify the table
						delete (pvfTables.at(j));
						pvfTables.erase(pvfTables.begin() + j);
						--i;
						--pvfTablesSize;
					}
				}
			}
			if (((i + 1) < pvfTablesSize) && (!pvfTables.at(i)->dst_src)
					&& (pvfTables.at(i + 1)->clock_cycle
							== pvfTables.at(i)->clock_cycle)) {
//				printf("delete =%i\n", pvfTables.at(i)->id);
//				pvf_table_delete(db, pvfTables.at(i), tot_reg_id); do not modify the table
				delete (pvfTables.at(i));
				pvfTables.erase(pvfTables.begin() + i);
				--pvfTablesSize;
			} else {
//				pvf_table_update(db, pvfTables.at(i), tot_reg_id); do not modify the table
			}
			tempPvf = tempPvf2;
		}
		/// AVF graph for pvfTables now!!

		PvfTable* pvfTable0 = pvfTables.at(0);
		int i = pvfTable0->clock_cycle / avf_range;
		int n_samples = 0;

		float avf_sum = 0;
		for (int z = 0; z < i; ++z) {
			//						printf("AVF [%i,%i,%i][%i] = %.3f\n", cudaExec->id,
			//								shaderId, warpId, (z + 1) * avf_range - 1, 0);

			//////////////////////////////////
			n_samples++;
			if ((n_samples - 1) >= AVFs_size) {
				printf("error pvf 0 = %i\n", (n_samples - 1));
				fflush(stdout);
				assert(0);
			} else {
				omp_set_lock(&openmp_lock);

				AVFs[n_samples - 1] += (avf_sum / avf_range);

				omp_unset_lock(&openmp_lock);
				if (AVFs[n_samples - 1] < 0) {
					printf("error pvf 0 = %f,%f\n", AVFs[n_samples - 1],
							(avf_sum / avf_range));
					fflush(stdout);
					assert(0);
				}
			}
			avf_sum = 0;
			/////////////////////////////////
		}
		int range = (i + 1) * avf_range;

		for (int i = 1; i < pvfTables.size(); ++i) {
			PvfTable* pvfTable1 = pvfTables.at(i);
			PvfTable* pvfTable2 = pvfTables.at(i - 1);
			if (pvfTable1->clock_cycle < range) {
//																			printf("Entering < range\n");

				avf_sum += pvfTable2->reg_pvf
						* (pvfTable1->clock_cycle
								- maxValue(pvfTable2->clock_cycle,
										(long long int) range - avf_range));
//																			printf("AVF [%lli - %i] = %.3f\n",
//																					maxValue(pvfTable2->clock_cycle,
//																							(long long int) range - avf_range),
//																					pvfTable1->clock_cycle,
//																					pvfTable2->reg_pvf);
			} else {
//																			printf("Entering else range\n");

				avf_sum += pvfTable2->reg_pvf
						* (range
								- maxValue(pvfTable2->clock_cycle,
										(long long int) range - avf_range));
//																			printf("AVF [%i - %lli] = %.3f\n",
//																					maxValue(pvfTable2->clock_cycle,
//																							(long long int) range - avf_range),
//																					range - 1, pvfTable2->reg_pvf);
//																			printf("AVF Sum = %.3f\n", avf_sum / avf_range);
				//////////////////////////////////
				n_samples++;
//				if ((n_samples - 1) >= AVFs_size) {
//					printf("error pvf 1 = %i\n", (n_samples - 1));
//					fflush(stdout);
//					assert(0);
//				} else {
				omp_set_lock(&openmp_lock);
				AVFs[n_samples - 1] += avf_sum / avf_range;
				omp_unset_lock(&openmp_lock);
//					if (AVFs[n_samples - 1] < 0) {
//						printf("error pvf 1 = %f,%f\n", AVFs[n_samples - 1],
//								(avf_sum / avf_range));
//						fflush(stdout);
//						assert(0);
//					}
//				}
				avf_sum = 0;
				/////////////////////////////////

				range += avf_range;
				--i;
			}

		}
		//					printf("AVF Sum = %.3f\n", avf_sum / avf_range);
		//////////////////////////////////
		n_samples++;
//		if ((n_samples - 1) >= AVFs_size) {
//			printf("error pvf 2 = %i\n", (n_samples - 1));
//			fflush(stdout);
//			assert(0);
//		} else {
		omp_set_lock(&openmp_lock);
		AVFs[n_samples - 1] += avf_sum / avf_range;
		omp_unset_lock(&openmp_lock);
//			if (AVFs[n_samples - 1] < 0) {
//				printf("error pvf 2 = %f,%f\n", AVFs[n_samples - 1],
//						(avf_sum / avf_range));
//				fflush(stdout);
//				assert(0);
//			}
//		}
		avf_sum = 0;
		/////////////////////////////////

		/// Freeing the vector:
		for (int i = 0; i < pvfTables.size(); ++i) {
			delete (pvfTables.at(i));
		}
	}
}

void print_help() {
	printf("Typical calls: ./Debug/Cpp_sqlite3 10 0 | tee mystdout_exec.txt\n");
	printf("ten means the avf range.\n");
	printf("zero means call everything\n");
	printf(
			"to run execution 2 only: ./Debug/Cpp_sqlite3 10 2 | tee mystdout_exec.txt\n");
	printf(
			"if you run in buggy mode (Wrong tot_sid,tot_wid), optimize it using ./Debug/Cpp_sqlite3 x -1 | tee mystdout_exec.txt\n");
	printf(
			"The third parameter is table execution range. For example ./Debug/Cpp_sqlite3 10 4 6 = means that it's from table 4 to 6 (inst and avf). \n You can set as 999 for execution to the end\n");

	fflush(stdout);
}

int main(int argc, char* argv[]) {
	printf("Number of argument = %i\n", argc);
	fflush(stdout);
	if (argc < 2) {
		printf("Please insert the avf_range as the second argument!\n");
		print_help();
		return -1;
	}
	avf_range = 10; // Sampling interval
	for (int var = 0; var < argc; ++var) {
		printf("arg[%i]=%s\n", var, argv[var]);

	}
	avf_range = atoi(argv[1]);
	if (avf_range < 10) {
		printf("Range is less than 10, set it bigger!\n");
		print_help();
		return -1;
	} else {
		printf("Executing with avf_range = %i\n", avf_range);
	}
	int cuda_execute_id = 0;
	if (argc > 2) {
		cuda_execute_id = atoi(argv[2]);
		if (cuda_execute_id == 0) {
			printf("Zero means call everything\n");
		} else {
			printf("Running execution %i only!\n", cuda_execute_id);
		}
	}

	//////////////////////////////////////////////////////////////
	if (pthread_mutex_init(&avf_lock, NULL) != 0) {
		printf("\n mutex init has failed\n");
		assert(0);
	}
	omp_init_lock(&openmp_lock);

	printf("============== Processing AVF ===============\n");

	auto start = std::chrono::system_clock::now();
//	system("rm cuda_kernel.sqlite3");
//	system("cp cuda_kernel_lstm10.sqlite3 cuda_kernel.sqlite3");
	avf_database_open(&db);
	std::vector<CudaExec *> cudaExecs;
	cuda_exec_read_list(db, &cudaExecs);
	int cudaExecsSize = cudaExecs.size();
	printf("Total kernel execution = %i\n", cudaExecsSize);
	fflush(stdout);
	if (argc > 3) {
		int cudaExecsSize_temp = atoi(argv[3]);
		if (cudaExecsSize_temp < cudaExecsSize) {
			cudaExecsSize = cudaExecsSize_temp;
		}
		printf("Exec range is set to %i\n", cudaExecsSize);

	}
	fflush(stdout);
	/////////////////// Optimizing the database
	/**
	 * Check if it's already optimized!
	 */
	///////////////////////////
	int cudaExecI = 0;
	// For execute the specific execute id:
	if (cuda_execute_id > 0) {
		cudaExecI = cuda_execute_id - 1;
	}
	///////////////////////////
	for (; cudaExecI < cudaExecsSize; ++cudaExecI) {
		if (!inst_pvf_table_is_optimized(db, cudaExecs.at(cudaExecI))) {
			printf("Optimizing the inst table:%i\n",
					cudaExecs.at(cudaExecI)->id);
			fflush(stdout);
			inst_pvf_table_optimize(db, cudaExecs.at(cudaExecI));
		}
		if (!pvf_table_is_optimized(db, cudaExecs.at(cudaExecI))) {
			printf("Optimizing the reg table:%i\n",
					cudaExecs.at(cudaExecI)->id);
			fflush(stdout);
			pvf_table_optimize(db, cudaExecs.at(cudaExecI));
		}
	}

	printf("Database optimized! Proceeding...\n");
	fflush(stdout);

	cudaExecI = 0;
	///////////////////////////
	// For execute the specific execute id:
	if (cuda_execute_id > 0) {
		cudaExecI = cuda_execute_id - 1;
	}
	///////////////////////////
	for (; cudaExecI < cudaExecsSize; ++cudaExecI) {

		CudaExec *cudaExec = cudaExecs.at(cudaExecI);
		printf("Processing exec[%i] = %s\n", cudaExec->id,
				cudaExec->getCudaKernel(db)->kernel_name.c_str());
		/*
		 * Bugfix mode
		 */
		// Get tot sid & wid
//		if (cuda_execute_id == -1) {
			inst_pvf_table_tot_sid_tot_wid(db, cudaExec);
			cuda_exec_update(db, cudaExec);
			printf("Fixing tot_sid,tot_wid to %i,%i\n", cudaExec->tot_sid,
					cudaExec->tot_wid);
//		}

//		max_clockcycle += avfSamplingInterval;
//		printf("Max clockcycle = %i\n", max_clockcycle);
		int tot_block_id = cudaExec->gridDim_x * cudaExec->gridDim_y
				* cudaExec->gridDim_z;
		printf("total_block = %i \n", tot_block_id);
		int tot_thread_id = cudaExec->blockDim_x * cudaExec->blockDim_y
				* cudaExec->blockDim_z;
		printf("total_thread = %i \n", tot_thread_id);

		int tot_reg_id = 0;
		long long max_clock_cycle = 0;
		pvf_table_get_max_reg_id_max_clock_cycle(db, cudaExec->id, &tot_reg_id,
				&max_clock_cycle);
		tot_reg_id++;
		printf("Max clockcycle = %lli\n", max_clock_cycle);
		sqlite3_exec(db, "BEGIN TRANSACTION;", NULL, NULL, NULL);
		{ // Scope for instruction processes
			std::ofstream inst_avf_file;
			std::string inst_avf_file_name = "inst_avf_file_";
			inst_avf_file_name.append(std::to_string(cudaExec->id));
			inst_avf_file_name.append(".csv");
			inst_avf_file.open(inst_avf_file_name.c_str());
			int instAVFs_size = (max_clock_cycle / avf_range) + 1 + 1; // because max_clock_cycle / avf_range is a floor, +1, then the size must be +1 again
			float instAVFs[instAVFs_size];
			memset(instAVFs, 0, instAVFs_size * sizeof(float));
			for (int shaderId = 1; shaderId < cudaExec->tot_sid; ++shaderId) {
#pragma omp parallel for
				for (int warpId = 0; warpId < cudaExec->tot_wid; ++warpId) {
					processInstructions(cudaExec->id, shaderId, warpId,
							instAVFs, instAVFs_size);
				}
			}

			/**
			 * Note: Warp in a shader = 16
			 */
			for (int i = 0; i < instAVFs_size; ++i) {
				inst_avf_file << (i + 1) * avf_range << "," << std::fixed
						<< std::setprecision(4) << instAVFs[i] << "\n";
				printf("Inst AVF[%i] are = %.6f\n", (i + 1) * avf_range,
						instAVFs[i]);
			}
			inst_avf_file.close();
		}
		{ // Scope for register processes
			std::ofstream avf_file;
			std::string avf_file_name = "avf_file_";
			avf_file_name.append(std::to_string(cudaExec->id));
			avf_file_name.append(".csv");
			avf_file.open(avf_file_name.c_str());
			int AVFs_size = (max_clock_cycle / avf_range) + 1 + 1; // because max_clock_cycle / avf_range is a floor, +1, then the size must be +1 again
			float AVFs[AVFs_size];
			memset(AVFs, 0, AVFs_size * sizeof(float));
			for (int gridId_z = 0; gridId_z < cudaExec->gridDim_z; ++gridId_z) {
				for (int gridId_y = 0; gridId_y < cudaExec->gridDim_y;
						++gridId_y) {
					for (int gridId_x = 0; gridId_x < cudaExec->gridDim_x;
							++gridId_x) {
						for (int blockId_z = 0;
								blockId_z < cudaExec->blockDim_z; ++blockId_z) {
							for (int blockId_y = 0;
									blockId_y < cudaExec->blockDim_y;
									++blockId_y) {
#pragma omp parallel for
								for (int blockId_x = 0;
										blockId_x < cudaExec->blockDim_x;
										++blockId_x) {
									for (int regId = 0; regId < tot_reg_id; //cudaExec->getCudaKernel(db)->numOfRegister;
											++regId) {
										processAVF(cudaExec->id,
												flatten3Dto1Dcta(gridId_x,
														gridId_y, gridId_z,
														cudaExec->gridDim_x,
														cudaExec->gridDim_y),
												flatten3Dto1Dtid(blockId_x,
														blockId_y, blockId_z,
														cudaExec->blockDim_x,
														cudaExec->blockDim_y),
												regId, tot_block_id,
												tot_thread_id, tot_reg_id //cudaExec->getCudaKernel(db)->numOfRegister
												, AVFs, AVFs_size);

									}
								}
							}
						}
					}
				}
			}
			for (int i = 0; i < AVFs_size; ++i) {
				avf_file << (i + 1) * avf_range << "," << std::fixed
						<< std::setprecision(4) << AVFs[i] << "\n";
				printf("AVF[%i] are = %.6f\n", (i + 1) * avf_range, AVFs[i]);
			}
			avf_file.close();
		}
		sqlite3_exec(db, "END TRANSACTION;", NULL, NULL, NULL);
//		printf("Max clock_cycle = %lli\n", max_clockcycle);
		///////////////////////////
		// For execute the specific execute id:
		if ((argc == 2) && (cuda_execute_id > 0)) {
			break;
		}
		///////////////////////////
	}

	avf_database_close(db);
	auto finish = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsedTime = finish - start;
	printf("Time = %f Sec\n", elapsedTime.count());
	printf("===================== Done !!! =================\n");
}
