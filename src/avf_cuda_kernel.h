/*
 * avf_cuda_kernel.h
 *
 *  Created on: Mar 21, 2019
 *      Author: therangersolid
 */

#ifndef AVF_CUDA_KERNEL_H_
#define AVF_CUDA_KERNEL_H_

#include <sqlite3.h>
#include <string>
class CudaKernel{
public:
	CudaKernel();

	int id;
	std::string kernel_name;
	int numOfRegister;
};

extern pthread_mutex_t avf_lock; // implement this as well!

/**
 * Open the database
 */
void avf_database_open(sqlite3** db);

/**
 * Close the database
 */
void avf_database_close(sqlite3* db);

/**
 * Fill the cudaKernel's data with the given id!
 */
int cuda_kernel_read(sqlite3* db,CudaKernel* cudaKernel);

/**
 * Initialize the database table: Create the table for the first time
 */
int cuda_kernel_create(sqlite3* db);

/**
 * Return the sqlite's status like SQLITE_DONE. If it's SQLITE_DONE, the id of the cuda_kernel will be filled with the newly inserted rows primary keys
 * When you insert, leave the id to 0
 */
int cuda_kernel_insert(sqlite3* db, CudaKernel* cudaKernel);

int cuda_kernel_getid_by_kernel_name(sqlite3* db, CudaKernel* cudaKernel);

#endif /* AVF_CUDA_KERNEL_H_ */
