/*
 * avf_instruction_table.cc
 *
 *  Created on: Mar 31, 2019
 *      Author: gpgpu-sim
 */

#include "avf_instruction_table.h"
#include <sqlite3.h>

#include <iostream>
#include <string>
#include <assert.h>
#include <stdio.h>

/**
 * This will create a pvf table with the following convention
 * pvf_table_<cuda_exec_id>_<shader_id>_<warp_id>
 */
int inst_pvf_table_create(sqlite3* db, int cuda_exec_id) {

	int rc = SQLITE_DONE;

	sqlite3_stmt *stmt;

	std::string sql = "	CREATE TABLE inst_pvf_table_";
	sql.append(std::to_string(cuda_exec_id));
	sql.append(" ("
			"		`id`	INTEGER NOT NULL PRIMARY KEY,"
			"		`clock_cycle`	INTEGER NOT NULL,"
			"		`dst_src`	NUMERIC NOT NULL,"
			"		`reg_pvf`	INTEGER NOT NULL,"
			"		`shader_id`	INTEGER NOT NULL,"
			"		`warp_id`	INTEGER NOT NULL"
			"			);");
	sqlite3_prepare_v2(db, sql.c_str(), -1, &stmt, NULL);

	rc = sqlite3_step(stmt);
	if (rc != SQLITE_DONE) {
		printf("\nCould not step (execute) stmt: %s\n", sqlite3_errmsg(db));
		assert(0);
		return 1;
	}
	rc = sqlite3_finalize(stmt);
	return rc;
}

int inst_pvf_table_read_list(sqlite3* db,
		std::vector<InstPvfTable*>* instPvfTables, int cuda_exec_id,
		int shader_id, int warp_id) {
	CudaExec cudaExec;
	cudaExec.id = cuda_exec_id;
	int rc = 0;
	sqlite3_stmt *stmt;

	std::string sql =
			"SELECT id,clock_cycle,dst_src,reg_pvf FROM inst_pvf_table_";
	sql.append(std::to_string(cuda_exec_id));
	sql.append(" WHERE shader_id=");
	sql.append(std::to_string(shader_id));
	sql.append(" AND warp_id=");
	sql.append(std::to_string(warp_id));
	sql.append(";");
//	sql.append(" ORDER BY clock_cycle, dst_src ASC;"); No deed to order by! it's already compacted

	sqlite3_prepare_v2(db, sql.c_str(), -1, &stmt, NULL);
	bool done = false;
	while (!done) {
		switch (sqlite3_step(stmt)) {
		case SQLITE_ROW: {

			InstPvfTable *instPvfTable = new InstPvfTable(
					sqlite3_column_int64(stmt, 1), sqlite3_column_int(stmt, 2),
					sqlite3_column_double(stmt, 3), false);

			instPvfTable->id = sqlite3_column_int64(stmt, 0);
			instPvfTable->setCudaExec(&cudaExec);
			instPvfTables->push_back(instPvfTable);
			instPvfTable->shader_id = shader_id;
			instPvfTable->warp_id = warp_id;
		}
			break;
		case SQLITE_DONE:
			done = true;
			break;
		default:
			printf("Failed.\n");
			printf("%s\n", sqlite3_errmsg(db));
			fflush (stdout);
			assert(0);
		}
	}
	sqlite3_finalize(stmt);
	return rc;
}
//
///*
// * Will get. Therefore, the next cycle should read this value
// */
//void inst_pvf_table_get_sum_ranged(sqlite3* db,std::vector<InstPvfTable*>* instPvfTables,int cuda_exec_id, int shader_id, int warp_id, int lower_limit, int upper_limit) {
//	float sum = 0;
//	CudaExec cudaExec;
//	cudaExec.id = cuda_exec_id;
//	sqlite3_stmt *stmt;
//
//	std::string sql = "SELECT id,clock_cycle,dst_src,reg_pvf FROM inst_pvf_table_";
//	sql.append(std::to_string(cuda_exec_id));
//	sql.append("_");
//	sql.append(std::to_string(shader_id));
//	sql.append("_");
//	sql.append(std::to_string(warp_id));
//	sql.append(" WHERE clock_cycle > ");
//	sql.append(std::to_string(lower_limit));
//	sql.append(" AND clock_cycle < ");
//	sql.append(std::to_string(upper_limit));
//	sql.append(" ORDER BY clock_cycle, dst_src ASC;");
//
//	sqlite3_prepare_v2(db, sql.c_str(), -1, &stmt, NULL);
//	bool done = false;
//	while (!done) {
//		switch (sqlite3_step(stmt)) {
//		case SQLITE_ROW: {
//			InstPvfTable *instPvfTable = new InstPvfTable(sqlite3_column_int64(stmt, 1),
//								sqlite3_column_int(stmt, 2),
//								sqlite3_column_double(stmt, 3));
//						instPvfTable->id = sqlite3_column_int(stmt, 0);
//						instPvfTable->setCudaExec(&cudaExec);
//						instPvfTables->push_back(instPvfTable);
//						instPvfTable->shader_id = shader_id;
//						instPvfTable->warp_id = warp_id;
//		}
//			break;
//		case SQLITE_DONE:
//			done = true;
//			break;
//		default:
//			printf("Failed.\n");
//			printf(sqlite3_errmsg(db));
//			fflush(stdout);
//			assert(0);
//		}
//	}
//	sqlite3_finalize(stmt);
//	return sum;
//}

/**
 * Return true if it's already optimized, else false
 */
bool inst_pvf_table_is_optimized(sqlite3* db, CudaExec* cudaExec) {

	sqlite3_stmt *stmt;
	std::string sql = "PRAGMA index_list(inst_pvf_table_"; // Just check one is enough
	sql.append(std::to_string(cudaExec->id));
	sql.append(");");
	sqlite3_prepare_v2(db, sql.c_str(), -1, &stmt, NULL);
	bool is_optimized = false;
	bool done = false;
	while (!done) {
		switch (sqlite3_step(stmt)) {
		case SQLITE_ROW: {
			is_optimized = true;
		}
			break;
		case SQLITE_DONE:
			done = true;
			break;
		default:
			printf("Failed.\n");
			printf("\nCould not step (execute) stmt: %s\n", sqlite3_errmsg(db));
			fflush (stdout);
			assert(0);
		}
	}
	sqlite3_finalize(stmt);
	return is_optimized;
}

int inst_pvf_table_optimize(sqlite3* db, CudaExec* cudaExec) {

	int rc;
	printf("CREATE TABLE fixed_inst_pvf_table_%i... ", cudaExec->id);
	fflush (stdout);
	std::string sql = "CREATE TABLE fixed_inst_pvf_table_";
	sql.append(std::to_string(cudaExec->id));
	sql.append(" AS SELECT * from inst_pvf_table_");
	sql.append(std::to_string(cudaExec->id));
	sql.append(" ORDER BY shader_id, warp_id, clock_cycle, dst_src ASC;");
	rc = sqlite3_exec(db, sql.c_str(), NULL, 0, NULL);
	if (rc != SQLITE_OK) {
		printf("\nCould not step (execute) stmt: %s\n", sqlite3_errmsg(db));
		fflush(stdout);
		assert(0);
		return 1;
	}
	printf("Done!\n");
	printf("DROP TABLE inst_pvf_table_%i... ", cudaExec->id);
	fflush(stdout);
	sql = (" DROP TABLE inst_pvf_table_");
	sql.append(std::to_string(cudaExec->id));
	sql.append(" ;");
	rc = sqlite3_exec(db, sql.c_str(), NULL, 0, NULL);
	if (rc != SQLITE_OK) {
		printf("\nCould not step (execute) stmt: %s\n", sqlite3_errmsg(db));
		fflush(stdout);
		assert(0);
		return 1;
	}
	printf("Done!\n");
	printf("ALTER TABLE fixed_inst_pvf_table_%i... ", cudaExec->id);
	fflush(stdout);
	sql = (" ALTER TABLE fixed_inst_pvf_table_");
	sql.append(std::to_string(cudaExec->id));
	sql.append(" RENAME TO inst_pvf_table_");
	sql.append(std::to_string(cudaExec->id));
	sql.append(" ; ");
	rc = sqlite3_exec(db, sql.c_str(), NULL, 0, NULL);
	if (rc != SQLITE_OK) {
		printf("\nCould not step (execute) stmt: %s\n", sqlite3_errmsg(db));
		fflush(stdout);
		assert(0);
		return 1;
	}
	printf("Done!\n");
	printf("CREATE INDEX inst_pvf_index_%i... ", cudaExec->id);
	fflush(stdout);
	sql = (" CREATE INDEX inst_pvf_index_");
	sql.append(std::to_string(cudaExec->id));
	sql.append(" ON inst_pvf_table_");
	sql.append(std::to_string(cudaExec->id));
	sql.append(" (shader_id, warp_id);");

	rc = sqlite3_exec(db, sql.c_str(), NULL, 0, NULL);
	if (rc != SQLITE_OK) {
		printf("\nCould not step (execute) stmt: %s\n", sqlite3_errmsg(db));
		fflush(stdout);
		assert(0);
		return 1;
	}
	printf("Done!\n");
	fflush(stdout);
	return rc;
}

int inst_pvf_table_delete(sqlite3* db, InstPvfTable* instPvfTables) {
	int rc;
	sqlite3_stmt *stmt;
	std::string sql = "DELETE FROM inst_pvf_table_";
	sql.append(std::to_string(instPvfTables->getCudaExecId()));
	sql.append(" WHERE id=?1;");

	sqlite3_prepare_v2(db, sql.c_str(), -1, &stmt, NULL);
	sqlite3_bind_int64(stmt, 1, instPvfTables->id);
	rc = sqlite3_step(stmt);
	if (rc != SQLITE_DONE) {
		printf("\nCould not step (execute) stmt: %s\n", sqlite3_errmsg(db));
		assert(0);
		return 1;
	}
	sqlite3_finalize(stmt);
	return 0;
}

int inst_pvf_table_update(sqlite3* db, InstPvfTable* instPvfTables) {
	int rc;
	sqlite3_stmt *stmt;

	std::string sql = "UPDATE inst_pvf_table_";
	sql.append(std::to_string(instPvfTables->getCudaExecId()));
	sql.append(" SET clock_cycle=?2,dst_src=?3,reg_pvf=?4 WHERE id=?1;");

	sqlite3_prepare_v2(db, sql.c_str(), -1, &stmt, NULL);
	sqlite3_bind_int64(stmt, 1, instPvfTables->id);
	sqlite3_bind_int64(stmt, 2, instPvfTables->clock_cycle);
	sqlite3_bind_int(stmt, 3, instPvfTables->dst_src);
	sqlite3_bind_double(stmt, 4, instPvfTables->reg_pvf);
	rc = sqlite3_step(stmt);
	if (rc != SQLITE_DONE) {
		printf("\nCould not step (execute) stmt: %s\n", sqlite3_errmsg(db));
		assert(0);
		return 1;
	}
	sqlite3_finalize(stmt);
	return rc;
}

//Correcting the max warp id && shader id in the table
void inst_pvf_table_tot_sid_tot_wid(sqlite3* db, CudaExec* cudaExec) {
	int rc;
	sqlite3_stmt *stmt;

	std::string sql = "SELECT MAX(shader_id),MAX(warp_id) FROM inst_pvf_table_";
	sql.append(std::to_string(cudaExec->id));
	sql.append(";");

	sqlite3_prepare_v2(db, sql.c_str(), -1, &stmt, NULL);
	bool done = false;
	while (!done) {
		switch (sqlite3_step(stmt)) {
		case SQLITE_ROW: {
			cudaExec->tot_sid = sqlite3_column_int(stmt, 0) + 1;
			cudaExec->tot_wid = sqlite3_column_int(stmt, 1) + 1;
		}
			break;
		case SQLITE_DONE:
			done = true;
			break;
		default:
			printf("Failed.\n");
			printf("%s\n", sqlite3_errmsg(db));
			fflush (stdout);
			assert(0);
		}
	}
	sqlite3_finalize(stmt);
}

int inst_pvf_table_insert(sqlite3* db, InstPvfTable* instPvfTable) {
	int rc;
	sqlite3_stmt *stmt;

	std::string sql = "INSERT INTO inst_pvf_table_";
	sql.append(std::to_string(instPvfTable->getCudaExecId()));
	sql.append(
			" (clock_cycle,dst_src,reg_pvf,shader_id,warp_id) VALUES(?1,?2,?3,?4,?5)");
	sqlite3_prepare_v2(db, sql.c_str(), -1, &stmt, NULL);
	sqlite3_bind_int64(stmt, 1, instPvfTable->clock_cycle);
	sqlite3_bind_int(stmt, 2, instPvfTable->dst_src);
	sqlite3_bind_double(stmt, 3, instPvfTable->reg_pvf);
	sqlite3_bind_int(stmt, 4, instPvfTable->shader_id);
	sqlite3_bind_int(stmt, 5, instPvfTable->warp_id);
	rc = sqlite3_step(stmt);
	if (rc != SQLITE_DONE) {
		printf("\nCould not step (execute) stmt: %s\n", sqlite3_errmsg(db));
		printf("%i\n", rc);
		assert(0);
		return 1;
	} else {
		instPvfTable->id = sqlite3_last_insert_rowid(db);
		rc = sqlite3_finalize(stmt);
	}

	return rc;
}

InstPvfTable::InstPvfTable(long long clock_cycle, bool dst_src, float reg_pvf,
		bool check_dst_src) {

	this->id = 0;
	this->cudaExec = NULL;
	this->cuda_exec_id = 0;
	this->cuda_exec_allocated = false;
	this->clock_cycle = clock_cycle;
	this->dst_src = dst_src;
	if (dst_src && check_dst_src) { // if true, that means this is destination register
		this->reg_pvf = 0;
	} else {
		this->reg_pvf = reg_pvf;
	}
	this->shader_id = 0;
	this->warp_id = 0;
}

InstPvfTable::~InstPvfTable() {
	if (cuda_exec_allocated) {
		delete (cudaExec);
		cuda_exec_allocated = false;
	}
}

CudaExec* InstPvfTable::getCudaExec(sqlite3* db) {
	if (cuda_exec_id == 0) {
		return NULL;
	} else {
		if (cudaExec == NULL) {
			CudaExec* cudaExec = new CudaExec();
			cudaExec->id = cuda_exec_id;
			cuda_exec_read(db, cudaExec);
			this->cudaExec = cudaExec;
			this->cuda_exec_allocated = true;
		}
		return cudaExec;
	}
}

void InstPvfTable::setCudaExec(CudaExec* cudaExec, bool justID) {
	if (cuda_exec_allocated) {
		delete (this->cudaExec);
		cuda_exec_allocated = false;
	}
	cuda_exec_id = cudaExec->id;
	if (justID) {
		this->cudaExec = NULL;
	} else {
		this->cudaExec = cudaExec;
	}

}
