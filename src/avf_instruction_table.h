/*
 * avf_instruction_table.h
 *
 *  Created on: Mar 31, 2019
 *      Author: gpgpu-sim
 */

#ifndef AVF_INSTRUCTION_TABLE_H_
#define AVF_INSTRUCTION_TABLE_H_

#include "avf_cuda_exec.h"

class InstPvfTable {
private:
	///
	int cuda_exec_id;
	bool cuda_exec_allocated; // true if allocated at getCudaExec, otherwise false
	CudaExec* cudaExec; // Won't be allocated if you didn't call getCudaExec
	///

public:
	long long id;
	// To record that this register is used at which cycle
	long long clock_cycle; // uint64_t regShaderCycles;
	// True means destination, else source
	int dst_src; // If it's true, then the data is fetched into the buffer;
	// ACE bit. For instructions, it's always one (Ace bit 64), because we only have 1 instruction possible,
	// otherwise unknown behaviour occurs.
	float reg_pvf;

	int shader_id;
	int warp_id;

//	/// The variable below is not stored in the database!
//	bool regWide;// True means has regPvfHigh. It uses two registers instead of one (64 bit operations)
//	float regPvfHigh; // high ace bit. Only used when using avfAddToContainer functions!

	InstPvfTable(long long clock_cycle, bool dst_src, float reg_pvf, bool check_dst_src=true);
	virtual ~InstPvfTable();

	CudaExec* getCudaExec(sqlite3* db);

	/**
	 * This function stores the data.
	 */
	void setCudaExec(CudaExec* cudaExec,bool justID=true);

	int getCudaExecId() const {
		return cuda_exec_id;
	}
};

int inst_pvf_table_create(sqlite3* db, int cuda_exec_id);
int inst_pvf_table_insert(sqlite3* db, InstPvfTable* instPvfTable);
int inst_pvf_table_read_list(sqlite3* db, std::vector<InstPvfTable*>* instPvfTables,
		int cuda_exec_id, int shader_id, int warp_id);
int inst_pvf_table_update(sqlite3* db, InstPvfTable* instPvfTables) ;
int inst_pvf_table_delete(sqlite3* db, InstPvfTable* instPvfTables);
void inst_pvf_table_tot_sid_tot_wid(sqlite3* db, CudaExec* cudaExec) ;
bool inst_pvf_table_is_optimized(sqlite3* db,CudaExec* cudaExec);
int inst_pvf_table_optimize(sqlite3* db, CudaExec* cudaExec);
#endif /* AVF_INSTRUCTION_TABLE_H_ */
