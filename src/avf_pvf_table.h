/*
 * AVF_PVF_TABLE.h
 *
 *  Created on: Mar 21, 2019
 *      Author: therangersolid
 */

#ifndef AVF_PVF_TABLE_H_
#define AVF_PVF_TABLE_H_

#include <sqlite3.h>
#include "avf_cuda_exec.h"

/**
 * This is equivalent to class HvfPredictorObj, where it contains:
 * 1. Shader cycle of when it is used,
 * 2. Whether this is the destination or the source side register,
 * 3. The register's pvf.
 */
class PvfTable {
private:
	///
	int cuda_exec_id;
	bool cuda_exec_allocated; // true if allocated at getCudaExec, otherwise false
	CudaExec* cudaExec; // Won't be allocated if you didn't call getCudaExec
	///

public:
	long long id;
	// To record that this register is used at which cycle
	long long clock_cycle; // uint64_t regShaderCycles;
	// True means destination, else source
	int dst_src; // bool regDest;
	// ACE bit / total bit width
	float reg_pvf;
	// Which shader the code is executed?
	int shader_id;

	int block_id;
	int thread_id;
	int reg_id;

	/// The variable below is not stored in the database!
	bool regWide;// True means has regPvfHigh. It uses two registers instead of one (64 bit operations)
	float regPvfHigh; // high ace bit. Only used when using avfAddToContainer functions!

	PvfTable(long long clock_cycle, bool dst_src, float reg_pvf, bool check_dst_src=true);
	virtual ~PvfTable();

	CudaExec* getCudaExec(sqlite3* db);

	/**
	 * This function stores the data.
	 */
	void setCudaExec(CudaExec* cudaExec,bool justID=true);

	int getCudaExecId() const {
		return cuda_exec_id;
	}

};

/**
 * Initialize the database table: Create the table for the first time
 */
int pvf_table_create(sqlite3* db, int tot_cuda_exec_id);

/**
 * Return the sqlite's status like SQLITE_DONE. If it's SQLITE_DONE, the id of the cuda_kernel will be filled with the newly inserted rows primary keys
 * When you insert, leave the id to 0
 */
int pvf_table_insert(sqlite3* db, PvfTable* pvfTable, int numOfCTA,
		int numOfThread, int tot_reg_id);

int pvf_table_read_list(sqlite3* db, std::vector<PvfTable*>* pvfTables,
		int cuda_exec_id, int block_id, int thread_id, int reg_id);

/**
 * This will track the id's it used, and then update as necessary
 */
int pvf_table_update(sqlite3* db,PvfTable* pvfTable,int tot_reg_id);
bool pvf_table_is_optimized(sqlite3* db,CudaExec* cudaExec);
int pvf_table_optimize(sqlite3* db, CudaExec* cudaExec) ;
int pvf_table_delete(sqlite3* db, PvfTable* pvfTable, int tot_reg_id);
int pvf_table_get_max_reg_id_max_clock_cycle(sqlite3* db, int cuda_exec_id,int* max_reg_id, long long* max_clock_cycle);
#endif /* AVF_PVF_TABLE_H_ */
