/*
 * avf_cuda_kernel.cpp
 * A crud model
 *  Created on: Mar 21, 2019
 *      Author: therangersolid
 */
#include "avf_cuda_kernel.h"
#include "avf_predictor.h"
#include <sqlite3.h>
#include <stdio.h>
#include <assert.h>

void avf_database_open(sqlite3** db) {
	pthread_mutex_lock(&avf_lock);
	int rc = sqlite3_open("cuda_kernel.sqlite3", db);

	if (rc != SQLITE_OK) {
		printf("Can't open database: %s\n", sqlite3_errmsg(*db));
		assert(0);
	} else {
		printf("Opened database successfully\n");
	}
}

void avf_database_close(sqlite3* db) {
	int rc = sqlite3_close(db);
	if (rc != SQLITE_OK) {
		printf("Can't close database: %s\n", sqlite3_errmsg(db));
		assert(0);
	} else {
		printf("Closed database successfully\n");
	}
	pthread_mutex_unlock(&avf_lock);
}

int cuda_kernel_create(sqlite3* db) {
	int rc;
	char *sql;
	/* Create SQL statement */
	sql = "CREATE TABLE `cuda_kernel` ("
			"	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,"
			"	`kernel_name`	TEXT NOT NULL UNIQUE,"
			"	`numOfRegister`	INTEGER NOT NULL);";
	char *zErrMsg = 0;
	rc = sqlite3_exec(db, sql, NULL, NULL, &zErrMsg);

	if (rc != SQLITE_OK) {
		printf("SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
		return (rc);
	} else {
		printf("cuda_kernel created successfully\n");
	}
	return SQLITE_OK;
}

int cuda_kernel_insert(sqlite3* db, CudaKernel* cudaKernel) {
	int rc;
	sqlite3_stmt *stmt;
	sqlite3_prepare_v2(db,
			"INSERT INTO cuda_kernel (kernel_name,numOfRegister) VALUES(?1,?2);",
			-1, &stmt, NULL);
	sqlite3_bind_text(stmt, 1, cudaKernel->kernel_name.c_str(), -1,
			SQLITE_STATIC);
	sqlite3_bind_int(stmt, 2, cudaKernel->numOfRegister);
	rc = sqlite3_step(stmt);
	if (rc != SQLITE_DONE) {
		printf("\nCould not step (execute) stmt: %s\n", sqlite3_errmsg(db));
		return 1;
	}
	cudaKernel->id = sqlite3_last_insert_rowid(db);
	sqlite3_finalize(stmt);
	return rc;
}

int cuda_kernel_read(sqlite3* db, CudaKernel* cudaKernel) {
	int rc;
	sqlite3_stmt *stmt;
	sqlite3_prepare_v2(db,
			"SELECT kernel_name,numOfRegister FROM cuda_kernel WHERE id=?1;",
			-1, &stmt, NULL);
	sqlite3_bind_int(stmt, 1, cudaKernel->id);
	bool done = false;
	while (!done) {
		switch (sqlite3_step(stmt)) {
		case SQLITE_ROW:
		{
			cudaKernel->kernel_name = reinterpret_cast<const char*>(sqlite3_column_text(stmt, 0));
			cudaKernel->numOfRegister = sqlite3_column_int(stmt, 1);
		}
			break;
		case SQLITE_DONE:
			done = true;
			break;
		default:
			printf("Failed.\n");
			fflush(stdout);
			assert(0);
		}
	}
	sqlite3_finalize(stmt);
	return rc;
}

int cuda_kernel_getid_by_kernel_name(sqlite3* db, CudaKernel* cudaKernel) {
	int rc;
	sqlite3_stmt *stmt;
	sqlite3_prepare_v2(db, "SELECT id FROM  cuda_kernel WHERE kernel_name=?1;",
			-1, &stmt, NULL);
	sqlite3_bind_text(stmt, 1, cudaKernel->kernel_name.c_str(), -1,
			SQLITE_STATIC);
	bool done = false;
	while (!done) {
		switch (sqlite3_step(stmt)) {
		case SQLITE_ROW:
			cudaKernel->id = sqlite3_column_int(stmt, 0);
			break;
		case SQLITE_DONE:
			done = true;
			break;
		default:
			printf("Failed getting id:%s\n",cudaKernel->kernel_name.c_str());
			printf("Errmsg:%s\n",sqlite3_errmsg(db));
			fflush(stdout);
			assert(0);
		}
	}
	sqlite3_finalize(stmt);
	return rc;
}

int cuda_kernel_update(sqlite3* db) {
	function_unimplemented();
	return -1;
}

int cuda_kernel_delete(sqlite3* db) {
	function_unimplemented();
	return -1;
}

CudaKernel::CudaKernel() {
	this->id = 0;
	this->kernel_name = "";
	this->numOfRegister = 0;
}
