/*
 * avf_cuda_kernel.cpp
 * A crud model
 *  Created on: Mar 21, 2019
 *      Author: therangersolid
 */
#include "avf_cuda_kernel.h"
#include "avf_cuda_exec.h"
#include "avf_predictor.h"
#include <sqlite3.h>
#include <stdio.h>
#include <assert.h>
#include <vector>

int cuda_exec_create(sqlite3* db) {
	int rc;
	char *sql;
	/* Create SQL statement */
	sql = "CREATE TABLE `cuda_exec` ("
			"				`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,"
			"				`cuda_kernel_id`	INTEGER NOT NULL,"
			"				`gridDim_x`	INTEGER NOT NULL,"
			"				`gridDim_y`	INTEGER NOT NULL,"
			"				`gridDim_z`	INTEGER NOT NULL,"
			"				`blockDim_x`	INTEGER NOT NULL,"
			"				`blockDim_y`	INTEGER NOT NULL,"
			"				`blockDim_z`	INTEGER NOT NULL,"
			"				`tot_sid`	INTEGER NOT NULL,"
			"				`tot_wid`	INTEGER NOT NULL,"
			"				FOREIGN KEY(`cuda_kernel_id`) REFERENCES cuda_kernel(id));";
	char *zErrMsg = 0;
	rc = sqlite3_exec(db, sql, NULL, NULL, &zErrMsg);

	if (rc != SQLITE_OK) {
		printf("SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
		return (rc);
	} else {
		printf("cuda_exec created successfully\n");
	}
	return SQLITE_OK;
}

int cuda_exec_insert(sqlite3* db, CudaExec* cudaExec) {
	int rc;
	sqlite3_stmt *stmt;
	sqlite3_prepare_v2(db,
			"INSERT INTO cuda_exec (cuda_kernel_id,gridDim_x,gridDim_y,gridDim_z,blockDim_x,blockDim_y,blockDim_z,tot_sid,tot_wid) VALUES(?1,?2,?3,?4,?5,?6,?7,?8,?9);",
			-1, &stmt, NULL);
	sqlite3_bind_int(stmt, 1, cudaExec->getCudaKernelId());
	sqlite3_bind_int(stmt, 2, cudaExec->gridDim_x);
	sqlite3_bind_int(stmt, 3, cudaExec->gridDim_y);
	sqlite3_bind_int(stmt, 4, cudaExec->gridDim_z);
	sqlite3_bind_int(stmt, 5, cudaExec->blockDim_x);
	sqlite3_bind_int(stmt, 6, cudaExec->blockDim_y);
	sqlite3_bind_int(stmt, 7, cudaExec->blockDim_z);
	sqlite3_bind_int(stmt, 8, cudaExec->tot_sid);
	sqlite3_bind_int(stmt, 9, cudaExec->tot_wid);
	rc = sqlite3_step(stmt);
	if (rc != SQLITE_DONE) {
		printf("\nCould not step (execute) stmt: %s\n", sqlite3_errmsg(db));
		return 1;
	}
	cudaExec->id = sqlite3_last_insert_rowid(db);
	sqlite3_finalize(stmt);
	return rc;
}

int cuda_exec_read(sqlite3* db, CudaExec* cudaExec) {
	int rc;
	sqlite3_stmt *stmt;
	sqlite3_prepare_v2(db,
			"SELECT cuda_kernel_id,gridDim_x,gridDim_y,gridDim_z,blockDim_x,blockDim_y,blockDim_z,tot_sid,tot_wid FROM cuda_exec WHERE id=?1;",
			-1, &stmt, NULL);
	sqlite3_bind_int(stmt, 1, cudaExec->id);
	bool done = false;
	while (!done) {
		switch (sqlite3_step(stmt)) {
		case SQLITE_ROW: {
			CudaKernel cudaKernel;
			cudaKernel.id = sqlite3_column_int(stmt, 0);
			cudaExec->setCudaKernel(&cudaKernel);
			cudaExec->gridDim_x = sqlite3_column_int(stmt, 1);
			cudaExec->gridDim_y = sqlite3_column_int(stmt, 2);
			cudaExec->gridDim_z = sqlite3_column_int(stmt, 3);
			cudaExec->blockDim_x = sqlite3_column_int(stmt, 4);
			cudaExec->blockDim_y = sqlite3_column_int(stmt, 5);
			cudaExec->blockDim_z = sqlite3_column_int(stmt, 6);
			cudaExec->tot_sid = sqlite3_column_int(stmt, 7);
			cudaExec->tot_wid = sqlite3_column_int(stmt, 8);
		}
			break;
		case SQLITE_DONE:
			done = true;
			break;
		default:
			printf("Failed.\n");
			printf("\nCould not step (execute) stmt: %s\n", sqlite3_errmsg(db));
			fflush(stdout);
			assert(0);
		}
	}
	sqlite3_finalize(stmt);
	return rc;
}

int cuda_exec_read_list(sqlite3* db, std::vector<CudaExec*>* cudaExecs) {
	int rc;
	sqlite3_stmt *stmt;
	sqlite3_prepare_v2(db,
			"SELECT id,cuda_kernel_id,gridDim_x,gridDim_y,gridDim_z,blockDim_x,blockDim_y,blockDim_z,tot_sid,tot_wid FROM cuda_exec ORDER BY id ASC;",
			-1, &stmt, NULL);
	bool done = false;
	while (!done) {
		switch (sqlite3_step(stmt)) {
		case SQLITE_ROW: {
			CudaExec *cudaExec = new CudaExec();
			cudaExec->id = sqlite3_column_int(stmt, 0);
			CudaKernel cudaKernel;
			cudaKernel.id = sqlite3_column_int(stmt, 1);
			cudaExec->setCudaKernel(&cudaKernel);
			cudaExec->gridDim_x = sqlite3_column_int(stmt, 2);
			cudaExec->gridDim_y = sqlite3_column_int(stmt, 3);
			cudaExec->gridDim_z = sqlite3_column_int(stmt, 4);
			cudaExec->blockDim_x = sqlite3_column_int(stmt, 5);
			cudaExec->blockDim_y = sqlite3_column_int(stmt, 6);
			cudaExec->blockDim_z = sqlite3_column_int(stmt, 7);
			cudaExec->tot_sid = sqlite3_column_int(stmt, 8);
			cudaExec->tot_wid = sqlite3_column_int(stmt, 9);
			cudaExecs->push_back(cudaExec);
		}
			break;
		case SQLITE_DONE:
			done = true;
			break;
		default:
			printf("Failed.\n");
			printf("\nCould not step (execute) stmt: %s\n", sqlite3_errmsg(db));
			fflush(stdout);
			assert(0);
		}
	}
	sqlite3_finalize(stmt);
	return rc;
}

int cuda_exec_update(sqlite3* db, CudaExec* cudaExec) {
	int rc;
	sqlite3_stmt *stmt;
	sqlite3_prepare_v2(db,
			"UPDATE cuda_exec SET cuda_kernel_id=?2,gridDim_x=?3,gridDim_y=?4,gridDim_z=?5,blockDim_x=?6,blockDim_y=?7,blockDim_z=?8,tot_sid=?9,tot_wid=?10 WHERE id=?1;",
			-1, &stmt, NULL);
	sqlite3_bind_int(stmt, 1, cudaExec->id);
	sqlite3_bind_int(stmt, 2, cudaExec->getCudaKernelId());
	sqlite3_bind_int(stmt, 3, cudaExec->gridDim_x);
	sqlite3_bind_int(stmt, 4, cudaExec->gridDim_y);
	sqlite3_bind_int(stmt, 5, cudaExec->gridDim_z);
	sqlite3_bind_int(stmt, 6, cudaExec->blockDim_x);
	sqlite3_bind_int(stmt, 7, cudaExec->blockDim_y);
	sqlite3_bind_int(stmt, 8, cudaExec->blockDim_z);
	sqlite3_bind_int(stmt, 9, cudaExec->tot_sid);
	sqlite3_bind_int(stmt, 10, cudaExec->tot_wid);
	rc = sqlite3_step(stmt);
	if (rc != SQLITE_DONE) {
		printf("\nCould not step (execute) stmt: %s\n", sqlite3_errmsg(db));
		return 1;
	}
	sqlite3_finalize(stmt);
	return rc;
}

int cuda_exec_delete(sqlite3* db) {
	function_unimplemented();
	return -1;
}

CudaExec::CudaExec() {
	this->tot_sid = 1;
	this->tot_wid = 0;
	this->id = 0;
	this->cuda_kernel_id = 0;
	this->cudaKernel = NULL;
	this->gridDim_x = 0;
	this->gridDim_y = 0;
	this->gridDim_z = 0;
	this->blockDim_x = 0;
	this->blockDim_y = 0;
	this->blockDim_z = 0;
	this->cuda_kernel_allocated = false;

}

CudaExec::~CudaExec() {
	if (cuda_kernel_allocated) {
		delete (cudaKernel);
		cuda_kernel_allocated = false;
	}
}

CudaKernel* CudaExec::getCudaKernel(sqlite3* db) {
	if (cuda_kernel_id == 0) {
		return NULL;
	} else {
		if (cudaKernel == NULL) {
			CudaKernel* cudaKernel = new CudaKernel();
			cudaKernel->id = cuda_kernel_id;
			cuda_kernel_read(db, cudaKernel);
			this->cudaKernel = cudaKernel;
			this->cuda_kernel_allocated = true;
		}
		return cudaKernel;
	}

}

void CudaExec::setCudaKernel(CudaKernel* cudaKernel, bool justID) {
	if (cuda_kernel_allocated) {
		delete (this->cudaKernel);
		cuda_kernel_allocated = false;
	}
	cuda_kernel_id = cudaKernel->id;
	if (justID) {
		this->cudaKernel = NULL;
	} else {
		this->cudaKernel = cudaKernel;
	}

}
