/*
 * avf_cuda_exec.h
 *
 *  Created on: Mar 21, 2019
 *      Author: therangersolid
 */

#ifndef AVF_CUDA_EXEC_H_
#define AVF_CUDA_EXEC_H_

#include <sqlite3.h>
#include <string.h>
#include "avf_cuda_kernel.h"
#include <vector>

class CudaExec {
private:
	///
	int cuda_kernel_id;
	bool cuda_kernel_allocated;// true if allocated at getCudaKernel, otherwise false
	CudaKernel* cudaKernel; // Won't be allocated if you didn't call getCudaKernel
	///

public:
	int id;
	int gridDim_x;
	int gridDim_y;
	int gridDim_z;
	int blockDim_x;
	int blockDim_y;
	int blockDim_z;
	int tot_sid;
	int tot_wid;

	CudaExec();
	virtual ~CudaExec();

	CudaKernel* getCudaKernel(sqlite3* db);

	/**
	 * This function stores the data.
	 */
	void setCudaKernel(CudaKernel* cudaKernel,bool justID=true);

	int getCudaKernelId() const {
		return cuda_kernel_id;
	}

};

/**
 * Initialize the database table: Create the table for the first time
 */
int cuda_exec_create(sqlite3* db);

/**
 * This will read by id
 */
int cuda_exec_read(sqlite3* db,CudaExec* cudaExec);

int cuda_exec_read_list(sqlite3* db,std::vector<CudaExec*>* cudaExecs);

/**
 * Return the sqlite's status like SQLITE_DONE. If it's SQLITE_DONE, the id of the cuda_kernel will be filled with the newly inserted rows primary keys
 * When you insert, leave the id to 0
 */
int cuda_exec_insert(sqlite3* db, CudaExec* cudaExec);
int cuda_exec_update(sqlite3* db, CudaExec* cudaExec);

#endif /* AVF_CUDA_EXEC_H_ */
