/*
 * avf_predictor.h
 *
 *  Created on: Aug 17, 2018
 *      Author: gpgpu-sim
 *      All implemented at shader.cc
 */

#ifndef AVF_PREDICTOR_H_
#define AVF_PREDICTOR_H_
#include <vector>
#include <stdio.h>
#include "avf_pvf_table.h"

#define function_unimplemented() (printf("File:%s , Line:%i , Function: %s is not implemented yet!\n",__FILE__, __LINE__, __func__))

#endif /* AVF_PREDICTOR_H_ */
