/*
 * avf_cuda_kernel.cpp
 * A crud model
 *  Created on: Mar 21, 2019
 *      Author: therangersolid
 */
#include "avf_pvf_table.h"
#include "avf_predictor.h"
#include <sqlite3.h>
#include <stdio.h>
#include <iostream>
#include <thread>
#include <chrono>
#include <pthread.h>
#include <assert.h>
//For pointer
#include <stdint.h>
#include <inttypes.h>

#include <unistd.h>
#include <syscall.h>
#include <string>
#include <vector>
/**
 * This will create a pvf table with the following convention
 * pvf_table_<cuda_exec_id>_<block_id>_<thread_id>_<reg_id>
 */
int pvf_table_create(sqlite3* db, int cuda_exec_id) {
	int rc = SQLITE_DONE;
	sqlite3_stmt *stmt;
	std::string sql = "	CREATE TABLE pvf_table_";
	sql.append(std::to_string(cuda_exec_id));
	sql.append(" (`id`	INTEGER NOT NULL PRIMARY KEY,"
			"		`clock_cycle`	INTEGER NOT NULL,"
			"		`dst_src`	NUMERIC NOT NULL,"
			"		`reg_pvf`	INTEGER NOT NULL,"
			"		`shader_id`	INTEGER NOT NULL,"
			"		`block_id`	INTEGER NOT NULL,"
			"		`thread_id`	INTEGER NOT NULL,"
			"		`reg_id`	INTEGER NOT NULL);");

	rc = sqlite3_exec(db, sql.c_str(), NULL, 0, NULL);

	if (rc != SQLITE_OK) {
		printf("\nCould not step (execute): %s\n", sqlite3_errmsg(db));
		assert(0);
		return 1;
	}

	return rc;
}

int pvf_table_insert(sqlite3* db, PvfTable* pvfTable, int tot_block_id,
		int tot_thread_id, int tot_reg_id) {
	if (pvfTable->block_id >= tot_block_id || pvfTable->block_id < 0) {
		printf("pvf_table_insert block_id = %i\n", pvfTable->block_id);
		return 0;
	}
	if (pvfTable->thread_id >= tot_thread_id || pvfTable->thread_id < 0) {
		printf("pvf_table_insert thread_id = %i\n", pvfTable->thread_id);
		return 0;
	}
	if (pvfTable->reg_id >= tot_reg_id || pvfTable->reg_id < 0) {
		printf("pvf_table_insert reg_id = %i\n", pvfTable->reg_id);
		return 0;
	}

	int rc;

	std::string sql = "INSERT INTO pvf_table_";
	sql.append(std::to_string(pvfTable->getCudaExecId()));
	sql.append(
			" (clock_cycle,dst_src,reg_pvf,shader_id,block_id,thread_id,reg_id) VALUES(");

	sql.append(std::to_string(pvfTable->clock_cycle));
	sql.append(",");
	sql.append(std::to_string(pvfTable->dst_src));
	sql.append(",");
	sql.append(std::to_string(pvfTable->reg_pvf));
	sql.append(",");
	sql.append(std::to_string(pvfTable->shader_id));
	sql.append(",");
	sql.append(std::to_string(pvfTable->block_id));
	sql.append(",");
	sql.append(std::to_string(pvfTable->thread_id));
	sql.append(",");
	sql.append(std::to_string(pvfTable->reg_id));
	sql.append(");");

	rc = sqlite3_exec(db, sql.c_str(), NULL, 0, NULL);
	if (rc != SQLITE_OK) {
		printf("\nCould not step (execute) stmt: %s\n", sqlite3_errmsg(db));
		assert(0);
		return 1;
	}
	pvfTable->id = sqlite3_last_insert_rowid(db);
	return rc;
}

/*
 * Return the value of the range
 */
//float pvf_table_get_sum_ranged(sqlite3* db, int cuda_exec_id, int block_id,
//		int thread_id, int reg_id, int lower_limit, int upper_limit) {
//	float sum = 0;
//	CudaExec cudaExec;
//	cudaExec.id = cuda_exec_id;
//	int rc = 0;
//	sqlite3_stmt *stmt;
////	printf("Sum ranged %i,%i,%i,%i,%i,%i,\n",cuda_exec_id,block_id,thread_id,reg_id,lower_limit,upper_limit);
//	std::string sql = "SELECT SUM(reg_pvf) FROM pvf_table_";
//	sql.append(std::to_string(cuda_exec_id));
//	sql.append("_");
//	sql.append(std::to_string(block_id));
//	sql.append("_");
//	sql.append(std::to_string(thread_id));
//	sql.append("_");
//	sql.append(std::to_string(reg_id));
//	sql.append(" WHERE clock_cycle > ");
//	sql.append(std::to_string(lower_limit));
//	sql.append(" AND clock_cycle < ");
//	sql.append(std::to_string(upper_limit));
//	sql.append(";");
//
//	sqlite3_prepare_v2(db, sql.c_str(), -1, &stmt, NULL);
//	bool done = false;
//	while (!done) {
//		switch (sqlite3_step(stmt)) {
//		case SQLITE_ROW: {
//			sum = sqlite3_column_double(stmt, 0);
//		}
//			break;
//		case SQLITE_DONE:
//			done = true;
//			break;
//		default:
//			printf("Failed.\n");
//			printf(sqlite3_errmsg(db));
//			fflush(stdout);
//			assert(0);
//		}
//	}
//	sqlite3_finalize(stmt);
////	printf("Sum total %.6f,\n",sum);
//	return sum;
//}
/**
 * Return true if it's already optimized, else false
 */
bool pvf_table_is_optimized(sqlite3* db, CudaExec* cudaExec) {

	sqlite3_stmt *stmt;
	std::string sql = "PRAGMA index_list(pvf_table_"; // Just check one is enough
	sql.append(std::to_string(cudaExec->id));
	sql.append(");");
	sqlite3_prepare_v2(db, sql.c_str(), -1, &stmt, NULL);
	bool is_optimized = false;
	bool done = false;
	while (!done) {
		switch (sqlite3_step(stmt)) {
		case SQLITE_ROW: {
			is_optimized = true;
		}
			break;
		case SQLITE_DONE:
			done = true;
			break;
		default:
			printf("Failed.\n");
			printf("\nCould not step (execute) stmt: %s\n", sqlite3_errmsg(db));
			fflush(stdout);
			assert(0);
		}
	}
	sqlite3_finalize(stmt);
	return is_optimized;
}

//int pvf_table_optimize(sqlite3* db, CudaExec* cudaExec) {
//
//	int rc;
//
//	std::string sql ="CREATE TABLE fixed_pvf_table_";
//	sql.append(std::to_string(cudaExec->id));
//	sql.append(" AS SELECT * from pvf_table_");
//	sql.append(std::to_string(cudaExec->id));
//	sql.append(
//			" ORDER BY block_id, thread_id, reg_id, clock_cycle, dst_src ASC;");
//	sql.append(" DROP TABLE pvf_table_");
//	sql.append(std::to_string(cudaExec->id));
//	sql.append(" ; ALTER TABLE fixed_pvf_table_");
//	sql.append(std::to_string(cudaExec->id));
//	sql.append(" RENAME TO pvf_table_");
//	sql.append(std::to_string(cudaExec->id));
//	sql.append(" ; CREATE INDEX pvf_index_");
//	sql.append(std::to_string(cudaExec->id));
//	sql.append(" ON pvf_table_");
//	sql.append(std::to_string(cudaExec->id));
//	sql.append(" (block_id, thread_id, reg_id);");
//
//	rc = sqlite3_exec(db, sql.c_str(), NULL, 0, NULL);
//	if (rc != SQLITE_OK) {
//		printf("\nCould not step (execute) stmt: %s\n", sqlite3_errmsg(db));
//		assert(0);
//		return 1;
//	}
//	return rc;
//}

int pvf_table_optimize(sqlite3* db, CudaExec* cudaExec) {

	int rc;
	printf("CREATE TABLE fixed_pvf_table_%i... ", cudaExec->id);
	fflush(stdout);
	std::string sql = "CREATE TABLE fixed_pvf_table_";
	sql.append(std::to_string(cudaExec->id));
	sql.append(" AS SELECT * from pvf_table_");
	sql.append(std::to_string(cudaExec->id));
	sql.append(
			" ORDER BY block_id, thread_id, reg_id, clock_cycle, dst_src ASC;");
	rc = sqlite3_exec(db, sql.c_str(), NULL, 0, NULL);
	if (rc != SQLITE_OK) {
		printf("\nCould not step (execute) stmt: %s\n", sqlite3_errmsg(db));
		assert(0);
		return 1;
	}
	printf("Done!\n");
	printf("DROP TABLE pvf_table_%i... ", cudaExec->id);
	fflush(stdout);
	sql = (" DROP TABLE pvf_table_");
	sql.append(std::to_string(cudaExec->id));
	sql.append(" ;");
	rc = sqlite3_exec(db, sql.c_str(), NULL, 0, NULL);
	if (rc != SQLITE_OK) {
		printf("\nCould not step (execute) stmt: %s\n", sqlite3_errmsg(db));
		assert(0);
		return 1;
	}
	printf("Done!\n");
	printf("ALTER TABLE fixed_pvf_table_%i... ", cudaExec->id);
	fflush(stdout);
	sql = (" ALTER TABLE fixed_pvf_table_");
	sql.append(std::to_string(cudaExec->id));
	sql.append(" RENAME TO pvf_table_");
	sql.append(std::to_string(cudaExec->id));
	sql.append(" ;");
	rc = sqlite3_exec(db, sql.c_str(), NULL, 0, NULL);
	if (rc != SQLITE_OK) {
		printf("\nCould not step (execute) stmt: %s\n", sqlite3_errmsg(db));
		assert(0);
		return 1;
	}
	printf("Done!\n");
	printf("CREATE INDEX pvf_index_%i... ", cudaExec->id);
	fflush(stdout);
	sql = ("CREATE INDEX pvf_index_");
	sql.append(std::to_string(cudaExec->id));
	sql.append(" ON pvf_table_");
	sql.append(std::to_string(cudaExec->id));
	sql.append(" (block_id, thread_id, reg_id);");
	rc = sqlite3_exec(db, sql.c_str(), NULL, 0, NULL);
	if (rc != SQLITE_OK) {
		printf("\nCould not step (execute) stmt: %s\n", sqlite3_errmsg(db));
		assert(0);
		return 1;
	}
	printf("Done!\n");
	fflush(stdout);
	return rc;
}

/*
 * give back max_register id && clock cycle
 */
int pvf_table_get_max_reg_id_max_clock_cycle(sqlite3* db, int cuda_exec_id,
		int* max_reg_id, long long* max_clock_cycle) {
	int rc = 0;
	sqlite3_stmt *stmt;

	std::string sql = "SELECT MAX(reg_id),MAX(clock_cycle) FROM pvf_table_";
	sql.append(std::to_string(cuda_exec_id));

	sqlite3_prepare_v2(db, sql.c_str(), -1, &stmt, NULL);
	bool done = false;
	while (!done) {
		switch (sqlite3_step(stmt)) {
		case SQLITE_ROW: {
			*max_reg_id = sqlite3_column_int(stmt, 0);
			*max_clock_cycle = sqlite3_column_int64(stmt, 1);
		}
			break;
		case SQLITE_DONE:
			done = true;
			break;
		default:
			printf("Failed.\n");
			printf(sqlite3_errmsg(db));
			fflush(stdout);
			assert(0);
		}
	}
	sqlite3_finalize(stmt);
	return rc;
}

int pvf_table_read(sqlite3* db) {
	function_unimplemented();
	return -1;
}

int pvf_table_read_list(sqlite3* db, std::vector<PvfTable*>* pvfTables,
		int cuda_exec_id, int block_id, int thread_id, int reg_id) {
	CudaExec cudaExec;
	cudaExec.id = cuda_exec_id;
	int rc = 0;
	sqlite3_stmt *stmt;

	std::string sql =
			"SELECT id,clock_cycle,dst_src,reg_pvf,shader_id FROM pvf_table_";
	sql.append(std::to_string(cuda_exec_id));
	sql.append(" WHERE block_id=");
	sql.append(std::to_string(block_id));
	sql.append(" AND thread_id=");
	sql.append(std::to_string(thread_id));
	sql.append(" AND reg_id=");
	sql.append(std::to_string(reg_id));
	sql.append(";");
//	sql.append(" ORDER BY clock_cycle, dst_src ASC;"); // No need to order by since we're already compacting the sqlite3

	sqlite3_prepare_v2(db, sql.c_str(), -1, &stmt, NULL);
	bool done = false;
	while (!done) {
		switch (sqlite3_step(stmt)) {
		case SQLITE_ROW: {
			PvfTable *pvfTable = new PvfTable(sqlite3_column_int64(stmt, 1),
					sqlite3_column_int(stmt, 2),
					sqlite3_column_double(stmt, 3));
			pvfTable->id = sqlite3_column_int64(stmt, 0);
			pvfTable->setCudaExec(&cudaExec);
			pvfTables->push_back(pvfTable);
			pvfTable->block_id = block_id;
			pvfTable->thread_id = thread_id;
			pvfTable->reg_id = reg_id;
			pvfTable->shader_id = sqlite3_column_int(stmt, 4);
//			printf("pvf_table_read_list shader id %i\n",pvfTable->shader_id);
		}
			break;
		case SQLITE_DONE:
			done = true;
			break;
		default:
			printf("Failed.\n");
			printf("%s\n", sqlite3_errmsg(db));
			fflush(stdout);
			assert(0);
		}
	}
	sqlite3_finalize(stmt);
	return rc;
}

int pvf_table_update(sqlite3* db, PvfTable* pvfTable, int tot_reg_id) {
	if (pvfTable->reg_id >= tot_reg_id || pvfTable->reg_id < 0) {
		printf("pvf_table_update reg_id = %i\n", pvfTable->reg_id);
		return 0;
	}
	int rc;
	sqlite3_stmt *stmt;

	std::string sql = "UPDATE pvf_table_";
	sql.append(std::to_string(pvfTable->getCudaExecId()));
	sql.append(" SET clock_cycle=?2,dst_src=?3,reg_pvf=?4 WHERE id=?1;");

	sqlite3_prepare_v2(db, sql.c_str(), -1, &stmt, NULL);
	sqlite3_bind_int64(stmt, 1, pvfTable->id);
	sqlite3_bind_int64(stmt, 2, pvfTable->clock_cycle);
	sqlite3_bind_int(stmt, 3, pvfTable->dst_src);
	sqlite3_bind_double(stmt, 4, pvfTable->reg_pvf);
	rc = sqlite3_step(stmt);
	if (rc != SQLITE_DONE) {
		printf("\nCould not step (execute) stmt: %s\n", sqlite3_errmsg(db));
		assert(0);
		return 1;
	}
	sqlite3_finalize(stmt);
	return rc;
}

int pvf_table_delete(sqlite3* db, PvfTable* pvfTable, int tot_reg_id) {
	if (pvfTable->reg_id >= tot_reg_id || pvfTable->reg_id < 0) {
		printf("pvf_table_delete reg_id = %i\n", pvfTable->reg_id);
		return 0;
	}
	int rc;
	sqlite3_stmt *stmt;
	std::string sql = "DELETE FROM pvf_table_";
	sql.append(std::to_string(pvfTable->getCudaExecId()));
	sql.append(" WHERE id=?1;");

	sqlite3_prepare_v2(db, sql.c_str(), -1, &stmt, NULL);
	sqlite3_bind_int(stmt, 1, pvfTable->id);
	rc = sqlite3_step(stmt);
	if (rc != SQLITE_DONE) {
		printf("\nCould not step (execute) stmt: %s\n", sqlite3_errmsg(db));
		assert(0);
		return 1;
	}
	sqlite3_finalize(stmt);
	return 0;
}

PvfTable::PvfTable(long long clock_cycle, bool dst_src, float reg_pvf,
		bool check_dst_src) {
	this->id = 0;
	this->cudaExec = NULL;
	this->cuda_exec_id = 0;
	this->cuda_exec_allocated = false;
	this->clock_cycle = clock_cycle;
	this->dst_src = dst_src;
	if (dst_src && check_dst_src) { // if true, that means this is destination register
		this->reg_pvf = 0;
	} else {
		this->reg_pvf = reg_pvf;
	}
	this->shader_id = 0;
	this->block_id = 0;
	this->thread_id = 0;
	this->reg_id = 0;
	this->regWide = false;
	this->regPvfHigh = 0.0f;
}

PvfTable::~PvfTable() {
	if (cuda_exec_allocated) {
		delete (cudaExec);
		cuda_exec_allocated = false;
	}
}

CudaExec* PvfTable::getCudaExec(sqlite3* db) {
	if (cuda_exec_id == 0) {
		return NULL;
	} else {
		if (cudaExec == NULL) {
			CudaExec* cudaExec = new CudaExec();
			cudaExec->id = cuda_exec_id;
			cuda_exec_read(db, cudaExec);
			this->cudaExec = cudaExec;
			this->cuda_exec_allocated = true;
		}
		return cudaExec;
	}
}

void PvfTable::setCudaExec(CudaExec* cudaExec, bool justID) {
	if (cuda_exec_allocated) {
		delete (this->cudaExec);
		cuda_exec_allocated = false;
	}
	cuda_exec_id = cudaExec->id;
	if (justID) {
		this->cudaExec = NULL;
	} else {
		this->cudaExec = cudaExec;
	}

}

