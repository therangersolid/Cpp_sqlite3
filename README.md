This is the cpp file for calculating the avf
It uses OpenMP

Thi program will optimize the table (if not optimized) using following sequence (for each inst and reg pvf table):
// 
// //
// CREATE TABLE fixed_pvf_table_1 AS 
// SELECT * from pvf_table_1 ORDER BY block_id, thread_id, reg_id, clock_cycle, dst_src ASC;
// DROP TABLE pvf_table_1;
// ALTER TABLE fixed_pvf_table_1
//   RENAME TO pvf_table_1;
// 
// CREATE INDEX pvf_index_1 
// ON pvf_table_1 (block_id, thread_id, reg_id);
// //
// 
// //
// CREATE TABLE fixed_inst_pvf_table_1 AS 
// SELECT * from inst_pvf_table_1 ORDER BY shader_id, warp_id, clock_cycle, dst_src ASC;
// DROP TABLE inst_pvf_table_1;
// ALTER TABLE fixed_inst_pvf_table_1
//   RENAME TO inst_pvf_table_1;
// 
// CREATE INDEX inst_pvf_index_1 
// ON inst_pvf_table_1 (shader_id, warp_id);
// //
